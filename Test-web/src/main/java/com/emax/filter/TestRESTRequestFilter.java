/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emax.filter;

/**
 *
 * @author emax
 */
import com.emax.dto.SimpleResponse;
import com.emax.ejb.ApiKeyBean;
import static com.emax.hash.Hmac.hmacDigest;
import com.emax.model.ApiKey;
import static com.emax.shared.Constantes.ERROR_403;
import static com.emax.shared.Constantes.ESTADO_ERROR;
import static com.emax.shared.Constantes.SERVICE_KEY;
import static com.emax.shared.Constantes.VACIO;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
@PreMatching
public class TestRESTRequestFilter implements ContainerRequestFilter {

    private final static Logger log = Logger.getLogger(TestRESTRequestFilter.class.getName());
    @EJB
    ApiKeyBean apiKeyBean;

    @Override
    public void filter(ContainerRequestContext requestCtx) throws IOException {

        String path = requestCtx.getUriInfo().getPath();

        if (requestCtx.getRequest().getMethod().equals("OPTIONS")) {
            requestCtx.abortWith(Response.status(Response.Status.OK).build());

            return;
        }

        try {
            if (!path.equals("/swagger.json")) {
                String serviceKey = requestCtx.getHeaderString(SERVICE_KEY);

                String key = hmacDigest(serviceKey, "username1", "HmacSHA256");
                System.err.println("hmacDigest" + key);

                SimpleResponse<List<ApiKey>> simpleResponse = apiKeyBean.obtenerApiKey(key);
                System.err.println(simpleResponse);
                if (simpleResponse.getEstado() == VACIO || simpleResponse.getEstado() == ESTADO_ERROR) {
                    requestCtx.abortWith(Response.status(Response.Status.FORBIDDEN).entity(ERROR_403).build());
                    return;
                }
            }
        } catch (Exception e) {
            requestCtx.abortWith(Response.status(Response.Status.FORBIDDEN).entity(ERROR_403).build());

        }
    }
}
