/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emax.auth;

/**
 *
 * @author emax
 */
import com.emax.dto.SimpleResponse;
import com.emax.ejb.ApiKeyBean;
import static com.emax.hash.Hmac.hmacDigest;
import com.emax.model.ApiKey;
import static com.emax.shared.Constantes.ESTADO_EXITO;
import static com.emax.shared.Constantes.VACIO;
import java.util.List;
import javax.ejb.EJB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class TestAuthenticator {

    @EJB
    ApiKeyBean apiKeyBean;
    
    
    private final Logger logger = LogManager.getLogger(this.getClass());

    private static TestAuthenticator authenticator = null;

    public static TestAuthenticator getInstance() {
        if (authenticator == null) {
            authenticator = new TestAuthenticator();
        }

        return authenticator;
    }

  
    public boolean isServiceKeyValid(String serviceKey) {
        try {
            String key = hmacDigest(serviceKey, "username1", "HmacSHA256");

            SimpleResponse<List<ApiKey>> simpleResponse = apiKeyBean.obtenerApiKey(key);
            if (simpleResponse.getEstado() == VACIO) {
                return false;
            }
            return simpleResponse.getEstado() == ESTADO_EXITO;

        } catch (Exception e) {
            logger.error(e.getMessage(), e);

            return false;
        }

    }

}
