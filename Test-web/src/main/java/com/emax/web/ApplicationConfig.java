/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emax.web;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author emax
 */
@javax.ws.rs.ApplicationPath("api")
public class ApplicationConfig extends Application {

    
    
     public ApplicationConfig() {
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.0");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setBasePath("/Test-web/api/ipsResource");
        beanConfig.setResourcePackage("com.emax.web");
        beanConfig.setScan(true);
        beanConfig.setPrettyPrint(true);

    }
    
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        resources.add(ApiListingResource.class);
        resources.add(SwaggerSerializers.class);
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method. It is automatically
     * populated with all resources defined in the project. If required, comment
     * out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.emax.filter.TestRESTRequestFilter.class);
        resources.add(com.emax.filter.TestRESTResponseFilter.class);
        resources.add(com.emax.web.IpsResource.class);
     
    }

}
