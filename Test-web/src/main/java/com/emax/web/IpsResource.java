/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emax.web;

import static com.emax.shared.Constantes.ERROR_400;
import static com.emax.shared.Constantes.ERROR_403;
import static com.emax.shared.Constantes.ERROR_404;
import static com.emax.shared.Constantes.ERROR_500;
import static com.emax.shared.Constantes.URL_IPS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author emax
 */
@Path("ipsResource")
@RequestScoped
@Api(value = "/ipsResource", description = "Api")

public class IpsResource {

    private final Logger logger = LogManager.getLogger(this.getClass());

    @GET
    @Path("consulta/{cedula}")

    @ApiOperation(
            value = "Encontrar persona por cedula",
            notes = "Encontrar persona por cedula",
            response = Response.class
    )
    @ApiResponses({
        @ApiResponse(code = 404, message = ERROR_404),
        @ApiResponse(code = 400, message = ERROR_400),
        @ApiResponse(code = 500, message = ERROR_500),
        @ApiResponse(code = 403, message = ERROR_403)
    })
    public Response obtenerAsociado(
            @Context HttpHeaders httpHeaders,
            @ApiParam(value = "Cedula to lookup for", required = true)
            @PathParam("cedula") String cedula) throws IOException {
        logger.info("IN:[]", cedula);
        String urlParameters = "nro_cic=" + cedula + "&recuperar=Recuperar&elegir=&envio=ok";
        String targetURL = URL_IPS;
        URL url;
        HttpURLConnection connection = null;
        String regex = "\\d+";

        

        if (!cedula.matches(regex)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ERROR_400).build();

        }

        try {
            //Create connection
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length", ""
                    + Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            //Get Response	
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();

            String html = response.toString();
            Document doc = Jsoup.parse(html);
            JSONObject jsonParentObject = new JSONObject();
            int counter = 0;
            for (Element table : doc.select("table")) {
                JSONObject jsonObject = new JSONObject();
                Elements ths = table.select("th");
                Elements tds = table.select("td");
                if (ths.size() == tds.size()) {

                    for (int i = 0; i < ths.size(); i++) {
                        if (!"".equals(tds.get(i).text())) {
                            jsonObject.put(ths.get(i).text(), tds.get(i).text());
                        }
                    }
                    if (counter == 0) {
                        jsonParentObject.put("persona", jsonObject);
                        counter++;
                    } else {
                        jsonParentObject.put("empleador", jsonObject);
                    }
                }

            }

            if (jsonParentObject.isEmpty()) {
                return Response.status(Response.Status.NOT_FOUND).entity(ERROR_404.replace("XXXXX", cedula)).build();

            }
            return Response.ok(jsonParentObject, MediaType.APPLICATION_JSON).build();

        } catch (Exception e) {

            e.printStackTrace();
            return Response.serverError().entity(ERROR_500).build();

        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }
    }

}
