package com.emax.mapper;

import com.emax.model.ApiKey;
import com.emax.model.ApiKeyExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface ApiKeyMapper {
    @SelectProvider(type=ApiKeySqlProvider.class, method="countByExample")
    long countByExample(ApiKeyExample example);

    @DeleteProvider(type=ApiKeySqlProvider.class, method="deleteByExample")
    int deleteByExample(ApiKeyExample example);

    @Delete({
        "delete from api_key",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into api_key (api_key)",
        "values (#{apiKey,jdbcType=VARCHAR})"
    })
    @Options(useGeneratedKeys=true,keyProperty="id")
    int insert(ApiKey record);

    @InsertProvider(type=ApiKeySqlProvider.class, method="insertSelective")
    @Options(useGeneratedKeys=true,keyProperty="id")
    int insertSelective(ApiKey record);

    @SelectProvider(type=ApiKeySqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="api_key", property="apiKey", jdbcType=JdbcType.VARCHAR)
    })
    List<ApiKey> selectByExample(ApiKeyExample example);

    @Select({
        "select",
        "id, api_key",
        "from api_key",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="api_key", property="apiKey", jdbcType=JdbcType.VARCHAR)
    })
    ApiKey selectByPrimaryKey(Integer id);

    @UpdateProvider(type=ApiKeySqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") ApiKey record, @Param("example") ApiKeyExample example);

    @UpdateProvider(type=ApiKeySqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") ApiKey record, @Param("example") ApiKeyExample example);

    @UpdateProvider(type=ApiKeySqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(ApiKey record);

    @Update({
        "update api_key",
        "set api_key = #{apiKey,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(ApiKey record);
}