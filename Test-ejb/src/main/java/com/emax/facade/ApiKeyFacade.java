/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emax.facade;

import javax.ejb.Stateless;
import javax.inject.Inject;
import org.mybatis.cdi.Mapper;
import com.emax.mapper.ApiKeyMapper;
import com.emax.model.ApiKey;
import com.emax.model.ApiKeyExample;

/**
 *
 * @author emax
 */
@Stateless
public class ApiKeyFacade extends GenericFacade<ApiKey, Integer, ApiKeyExample, ApiKeyMapper> {

    @Inject
    @Mapper
    ApiKeyMapper mapper;

    @Override
    public ApiKeyMapper getMapper() {
        return mapper;
    }

}
