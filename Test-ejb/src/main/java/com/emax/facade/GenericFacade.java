/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emax.facade;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 *
 * @author emax
 * @param <P> PojoClass
 * @param <ID> PrimaryKeyClass
 * @param <E> ExampleClass
 * @param <M> MapperClass
 */
public abstract class GenericFacade<P, ID extends Serializable, E, M> {

    private final Class<P> pojoClass;
    private final Class<E> exampleClass;

    public GenericFacade() {
        Type genericSuperClass = getClass().getGenericSuperclass();

        ParameterizedType parametrizedType = null;
        while (parametrizedType == null) {
            if ((genericSuperClass instanceof ParameterizedType)) {
                parametrizedType = (ParameterizedType) genericSuperClass;
            } else {
                genericSuperClass = ((Class<?>) genericSuperClass).getGenericSuperclass();
            }
        }
        this.pojoClass = (Class<P>) parametrizedType.getActualTypeArguments()[0];
        this.exampleClass = (Class<E>) parametrizedType.getActualTypeArguments()[2];
    }

    public abstract M getMapper();

    public final int countByExample(final E example) throws Exception {
        M mapper = getMapper();
        Method m = mapper.getClass().getMethod("countByExample", exampleClass);
        int result = (Integer) m.invoke(mapper, example);
        return result;
    }

    public final int deleteByExample(final E example) throws Exception {
        M mapper = getMapper();
        Method m = mapper.getClass().getMethod("deleteByExample", exampleClass);
        int result = (Integer) m.invoke(mapper, example);
        return result;
    }

    public final int deleteByPrimaryKey(final ID id) throws Exception {
        M mapper = getMapper();
        Method m = mapper.getClass().getMethod("deleteByPrimaryKey", id.getClass());
        int result = (Integer) m.invoke(mapper, id);
        return result;
    }

    public final int insert(final P record) throws Exception {
        M mapper = getMapper();
        Method m = mapper.getClass().getMethod("insert", pojoClass);
        int result = (Integer) m.invoke(mapper, record);
        return result;
    }

    public final int insertSelective(final P record) throws Exception {
        M mapper = getMapper();
        Method m = mapper.getClass().getMethod("insertSelective", pojoClass);
        int result = (Integer) m.invoke(mapper, record);
        return result;
    }

    public final List<P> selectByExample(final E example) throws Exception {
        M mapper = getMapper();
        Method m = mapper.getClass().getMethod("selectByExample", exampleClass);
        List<P> result = (List<P>) m.invoke(mapper, example);
        return result;
    }

    public final P selectByPrimaryKey(final ID id) throws Exception {
        M mapper = getMapper();
        Method m = mapper.getClass().getMethod("selectByPrimaryKey", id.getClass());
        P result = (P) m.invoke(mapper, id);
        return result;
    }

    public final int updateByExampleSelective(final P record, final E example) throws Exception {
        M mapper = getMapper();
        Method m = mapper.getClass().getMethod("updateByExampleSelective", pojoClass, exampleClass);
        int result = (Integer) m.invoke(mapper, record, example);
        return result;
    }

    public final int updateByExample(final P record, final E example) throws Exception {
        M mapper = getMapper();
        Method m = mapper.getClass().getMethod("updateByExample", exampleClass);
        int result = (Integer) m.invoke(mapper, example);
        return result;
    }

    public final int updateByPrimaryKeySelective(final P record) throws Exception {
        M mapper = getMapper();
        Method m = mapper.getClass().getMethod("updateByPrimaryKeySelective", pojoClass);
        int result = (Integer) m.invoke(mapper, record);
        return result;
    }

    public final int updateByPrimaryKey(final P record) throws Exception {
        M mapper = getMapper();
        Method m = mapper.getClass().getMethod("updateByPrimaryKey", pojoClass);
        int result = (Integer) m.invoke(mapper, record);
        return result;
    }
}
