/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emax.shared;

/**
 *
 * @author emax
 */
public class Constantes {

    public static final int ESTADO_EXITO = 0;
    public static final int ESTADO_ERROR = -1;
    public static final int VACIO = 555;
    public static final String ERROR_404 = "{ \"codigo\": \"g100\", \"error\": \"usuario con cédula XXXXX no existe\" }";
    public static final String ERROR_400 = "{ \"codigo\": \"g101\", \"error\": \"Parámetros inválidos\" }";

    public static final String ERROR_500 = "{ \"codigo\": \"g102\", \"error\": \"Error interno del servidor\" }";
    public static final String ERROR_403 = "{ \"codigo\": \"g103\", \"error\": \"No autorizado\" }";

    public static final String URL_IPS = "http://servicios.ips.gov.py/consulta_asegurado/comprobacion_de_derecho_externo.php";

    public static final String SERVICE_KEY = "service_key";
    public static final String MENSAJE_EXITO = "OK";
    public static final String MENSAJE_ERROR = "ERROR";
    public static final String MENSAJE_ERROR_AUTH = "No se ha podido autenticar el usuario";
    public static final String MENSAJE_ERROR_PERMISO = "No tiene permiso para realizar esta accion";
    public static final String MENSAJE_ERROR_PARAMETROS = "Parametros Incorrectos";

}
