/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emax.shared;

import java.util.List;

/**
 *
 * @author emax
 */
public class Util {

    public static List slice(List l, Integer pagina, Integer cantReg) {

        int p = pagina > 0 ? --pagina : pagina;

        try {
            return l.subList(p * cantReg, (p * cantReg) + cantReg);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            try {
                return l.subList(p * cantReg, l.size());
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
                return null;
            }

        }

    }

}
