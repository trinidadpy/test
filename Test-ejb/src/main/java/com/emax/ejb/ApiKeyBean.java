/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emax.ejb;

import com.emax.facade.ApiKeyFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.emax.dto.SimpleResponse;
import com.emax.model.ApiKey;
import com.emax.model.ApiKeyExample;

import static com.emax.shared.Constantes.ESTADO_ERROR;
import static com.emax.shared.Constantes.ESTADO_EXITO;
import static com.emax.shared.Constantes.MENSAJE_ERROR;
import static com.emax.shared.Constantes.MENSAJE_EXITO;
import static com.emax.shared.Constantes.VACIO;

/**
 *
 * @author emax
 */
@Stateless
public class ApiKeyBean {

    private final Logger logger = LogManager.getLogger(this.getClass());

    @EJB
    ApiKeyFacade apiKeyFacade;

    public SimpleResponse<List<ApiKey>> listarApiKey() {

        SimpleResponse<List<ApiKey>> response;

        try {
            ApiKeyExample comXmpl = new ApiKeyExample();

            List<ApiKey> listaApiKey = apiKeyFacade.selectByExample(comXmpl);

            response = new SimpleResponse<>(listaApiKey, ESTADO_EXITO, MENSAJE_EXITO);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            response = new SimpleResponse<>(ESTADO_ERROR, MENSAJE_ERROR);
        }

        return response;

    }

    public SimpleResponse<List<ApiKey>> obtenerApiKey(String apiKey) {

        SimpleResponse<List<ApiKey>> response;
        System.err.println("apiKey" + apiKey);

        try {
            ApiKeyExample comXmpl = new ApiKeyExample();
            comXmpl.createCriteria().andApiKeyEqualTo(apiKey);

            List<ApiKey> listaApiKey = apiKeyFacade.selectByExample(comXmpl);

            if (listaApiKey.isEmpty()) {
                System.err.println("entro vacio");

                return new SimpleResponse<>(listaApiKey, VACIO, MENSAJE_EXITO);
            }
            response = new SimpleResponse<>(listaApiKey, ESTADO_EXITO, MENSAJE_EXITO);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            response = new SimpleResponse<>(ESTADO_ERROR, MENSAJE_ERROR);
        }

        return response;

    }

}
