/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emax.dto;

import java.util.List;
import com.emax.model.ApiKey;

/**
 *
 * @author emax
 */
public class ListarApiKeyResponse extends SimpleResponse<List<ApiKey>> {

    private Integer total;

    public ListarApiKeyResponse(int estado, String mensaje) {
        super(estado, mensaje);
    }

    public ListarApiKeyResponse(List<ApiKey> data, int estado, String mensaje) {
        super(data, estado, mensaje);
    }

    public ListarApiKeyResponse(List<ApiKey> data, int estado, String mensaje, Integer total) {
        super(data, estado, mensaje);
        this.total = total;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return String.valueOf(total) + ";" + super.toString();
    }

}
