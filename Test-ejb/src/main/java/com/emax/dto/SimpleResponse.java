/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emax.dto;

/**
 *
 * @author emax
 */
public class SimpleResponse<T> extends BaseResponse {

    private T data;

    public SimpleResponse(int estado, String mensaje) {
        super(estado, mensaje);
    }

    public SimpleResponse() {
    }

    public SimpleResponse(T data, int estado, String mensaje) {
        super(estado, mensaje);
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return super.toString() + ";" + String.valueOf(data);
    }

}
